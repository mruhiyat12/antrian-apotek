-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2021 at 02:36 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpu_login_loket`
--

-- --------------------------------------------------------

--
-- Table structure for table `antrian_obat_jadi`
--

CREATE TABLE `antrian_obat_jadi` (
  `id_transaksi` int(11) NOT NULL,
  `no_antrian` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT 0,
  `date_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian_obat_jadi`
--

INSERT INTO `antrian_obat_jadi` (`id_transaksi`, `no_antrian`, `status`, `date_time`) VALUES
(1, 999, 1, 23022021),
(2, 610, 1, 23022021),
(3, 611, 1, 23022021),
(4, 300, 1, 24022021),
(5, 301, 0, 24022021),
(6, 302, 0, 24022021),
(7, 303, 0, 24022021),
(8, 304, 0, 24022021),
(9, 1, 1, 25022021),
(10, 2, 1, 25022021),
(11, 3, 1, 25022021),
(12, 4, 1, 25022021),
(13, 5, 1, 25022021),
(14, 6, 1, 25022021),
(15, 7, 1, 25022021),
(16, 8, 1, 25022021),
(17, 9, 1, 25022021),
(18, 10, 1, 25022021),
(19, 11, 1, 25022021),
(20, 12, 1, 25022021),
(21, 13, 1, 25022021),
(22, 14, 1, 25022021),
(23, 15, 1, 25022021),
(24, 16, 1, 25022021),
(25, 17, 1, 25022021),
(26, 18, 1, 25022021),
(27, 299, 1, 25022021),
(28, 20, 0, 25022021),
(29, 21, 0, 25022021),
(30, 22, 0, 25022021),
(31, 23, 0, 25022021),
(32, 24, 0, 25022021),
(33, 25, 0, 25022021),
(34, 26, 0, 25022021),
(35, 27, 0, 25022021),
(36, 28, 0, 25022021),
(37, 29, 0, 25022021),
(38, 30, 0, 25022021),
(39, 31, 0, 25022021),
(40, 32, 0, 25022021),
(41, 33, 0, 25022021),
(42, 34, 0, 25022021),
(43, 35, 0, 25022021),
(44, 36, 0, 25022021),
(45, 37, 0, 25022021),
(46, 38, 0, 25022021),
(47, 39, 0, 25022021),
(48, 40, 0, 25022021),
(49, 41, 0, 25022021),
(50, 42, 0, 25022021),
(51, 43, 0, 25022021),
(52, 44, 0, 25022021),
(53, 45, 0, 25022021),
(54, 46, 0, 25022021),
(55, 47, 0, 25022021),
(56, 48, 0, 25022021),
(57, 49, 0, 25022021),
(58, 50, 0, 25022021),
(59, 51, 0, 25022021),
(60, 52, 0, 25022021),
(61, 53, 0, 25022021),
(62, 54, 0, 25022021),
(63, 55, 0, 25022021),
(64, 56, 0, 25022021),
(65, 57, 0, 25022021),
(66, 58, 0, 25022021),
(67, 59, 0, 25022021),
(68, 60, 0, 25022021),
(69, 61, 0, 25022021),
(70, 62, 0, 25022021),
(71, 63, 0, 25022021),
(72, 64, 0, 25022021),
(73, 65, 0, 25022021),
(74, 66, 0, 25022021),
(75, 67, 0, 25022021),
(76, 299, 0, 25022021),
(77, 1, 1, 27022021),
(78, 2, 1, 27022021),
(79, 3, 1, 27022021),
(80, 4, 1, 27022021),
(81, 5, 1, 27022021),
(82, 6, 1, 27022021),
(83, 7, 0, 27022021),
(84, 8, 0, 27022021),
(85, 9, 0, 27022021),
(86, 10, 0, 27022021);

-- --------------------------------------------------------

--
-- Table structure for table `antrian_obat_racikan`
--

CREATE TABLE `antrian_obat_racikan` (
  `id_transaksi` int(11) NOT NULL,
  `no_antrian` int(20) NOT NULL,
  `status` int(20) NOT NULL DEFAULT 0,
  `date_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian_obat_racikan`
--

INSERT INTO `antrian_obat_racikan` (`id_transaksi`, `no_antrian`, `status`, `date_time`) VALUES
(1, 1, 1, 22022021),
(2, 2, 0, 22022021),
(3, 3, 0, 22022021),
(4, 4, 0, 22022021),
(5, 5, 0, 22022021),
(6, 6, 0, 22022021),
(7, 7, 0, 22022021),
(8, 1, 1, 23022021),
(9, 2, 0, 23022021),
(10, 3, 0, 23022021),
(11, 4, 0, 23022021),
(12, 22, 1, 24022021),
(13, 299, 1, 24022021),
(14, 102, 1, 24022021),
(15, 999, 1, 24022021),
(16, 1, 1, 25022021),
(17, 2, 1, 25022021),
(18, 3, 1, 25022021),
(19, 4, 1, 25022021),
(20, 5, 1, 25022021),
(21, 6, 1, 25022021),
(22, 7, 1, 25022021),
(23, 8, 1, 25022021),
(24, 9, 1, 25022021),
(25, 10, 1, 25022021),
(26, 11, 1, 25022021),
(27, 12, 1, 25022021),
(28, 13, 0, 25022021),
(29, 14, 0, 25022021),
(30, 15, 0, 25022021),
(31, 16, 0, 25022021),
(32, 17, 0, 25022021),
(33, 18, 0, 25022021),
(34, 19, 0, 25022021),
(35, 20, 0, 25022021),
(36, 21, 0, 25022021),
(37, 22, 0, 25022021),
(38, 23, 0, 25022021),
(39, 24, 0, 25022021),
(40, 25, 0, 25022021),
(41, 26, 0, 25022021),
(42, 27, 0, 25022021),
(43, 28, 0, 25022021),
(44, 29, 0, 25022021),
(45, 30, 0, 25022021),
(46, 31, 0, 25022021),
(47, 1, 1, 27022021),
(48, 2, 1, 27022021),
(49, 3, 1, 27022021),
(50, 4, 1, 27022021),
(51, 5, 0, 27022021),
(52, 6, 0, 27022021),
(53, 7, 0, 27022021),
(54, 8, 0, 27022021),
(55, 9, 0, 27022021),
(56, 10, 0, 27022021);

-- --------------------------------------------------------

--
-- Table structure for table `image_slide`
--

CREATE TABLE `image_slide` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` varchar(256) NOT NULL,
  `img_slide` varchar(256) NOT NULL,
  `active` int(2) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_slide`
--

INSERT INTO `image_slide` (`id`, `title`, `content`, `img_slide`, `active`, `date_created`) VALUES
(7, '', '', 'F10000211.JPG', 1, 1614390180),
(10, '', '', 'F10000092.JPG', 0, 1614391721),
(11, '', '', 'F1000015.JPG', 0, 1614391726),
(12, '', '', 'F1020006.JPG', 0, 1614393200);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'Muhammad Ruhiyat', 'muhammadruhiyat020998@gmail.com', 'Capture.PNG', '$2y$10$HikVRXE54.fWAz7sVHyaO.s76S3G.NyhIuRWCZjc8HKyyUTQ6RTlm', 1, 1, 1603241365),
(2, 'dani', 'dani@gmail.com', 'default.jpg', '$2y$10$zcKO4UotAjik1KnAdIi1hOd8IBntZ1TKPD0PjkKXIYTY2r89L75R6', 2, 1, 1603276157),
(10, 'Muhammad Ruhiyat', 'muhammadruhiyat06@gmail.com', 'default.jpg', '$2y$10$uF5NQmliBefWpCodv0VZ4uZKw2Bdbori2lwM0fPJnyCnnbh.u3PSq', 2, 1, 1608076244),
(11, 'ahmad', 'ahmad@gmail.com', 'default.jpg', '$2y$10$UJr5gTKE9hYkLPS3qBSPDuUcMCL5JKCM1QlMHqm1vmY1KmhGwWYh2', 2, 1, 1614216312);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(7, 1, 2),
(14, 1, 4),
(15, 2, 4),
(19, 2, 2),
(20, 1, 3),
(21, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Page');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(7, 2, 'Change Password', 'user/changepassword', 'fas fa-fw fa-key', 1),
(10, 2, 'My Profile', 'user', 'fas fa-fw fa-user-circle', 1),
(13, 4, 'New Slide', 'page/new_slide', 'fas fa-fw fa-plus-circle', 1),
(14, 1, 'Dashboard', 'admin', 'fa fa-fw fa-tachometer-alt', 1),
(15, 1, 'Role', 'admin/role', 'fa fa-fw fa-user-tag', 1),
(16, 1, 'Account', 'admin/account_user', 'fa fa-fw fa-users', 1),
(17, 1, 'Add User', 'auth/registration', 'fas fa-fw fa-user-plus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `antrian_obat_jadi`
--
ALTER TABLE `antrian_obat_jadi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `antrian_obat_racikan`
--
ALTER TABLE `antrian_obat_racikan`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `image_slide`
--
ALTER TABLE `image_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_menu` (`menu_id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `antrian_obat_jadi`
--
ALTER TABLE `antrian_obat_jadi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `antrian_obat_racikan`
--
ALTER TABLE `antrian_obat_racikan`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `image_slide`
--
ALTER TABLE `image_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD CONSTRAINT `sub_menu` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
