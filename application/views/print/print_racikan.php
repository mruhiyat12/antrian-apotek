<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->

    <h4 class=" text-gray-900 text-center" style="font-family: sans-serif; margin-bottom: -4px; font-size: 50px;"><?= $header; ?></h4>
    <h5 class=" mb-4 text-gray-900 text-center" style="font-family: sans-serif; font-size: 50px;"><?= $title; ?></h5>
    <!-- akhir Page heading -->
    <!-- ambil antrian -->
    <div class="row " style="height: 600px;">
        <div class="col-md   " style="margin-top: -10px;">
            <div class="text-center text-gray-900 ">
                <h4 style="font-size: 50px;">Obat Racikan</h4>
                <h1 style="font-size: 350px;">B<?php echo $antrian_racikan['no_antrian']; ?>
                    <?php if ($antrian_racikan['no_antrian'] < 1) {
                        $antri = 0;
                    } else {
                        $antri = $antrian_racikan['no_antrian'];
                    }
                    ?></h1>
                <h5 class="text-gray-900" style="font-size: 50px;">Silahkan menunggu sampai dipanggil.</h5>
                <h5 class="text-gray-900" style="font-size:  45px;">Tanggal: <?= $jam; ?></h5>
            </div>
        </div>

    </div>
    <!-- akhir ambil antrian -->

</div>
<script>
    window.print();
    location = "<?= base_url() ?>antrian/ambil_antrian/";
</script>
<!-- End of Main Content -->