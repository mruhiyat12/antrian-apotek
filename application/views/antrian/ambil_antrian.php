<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->

    <h1 class=" mb-2 text-gray-900 text-center" style="font-family: 'Lobster', cursive;"><?= $title; ?></h1>
    <!-- akhir Page heading -->
    <!-- ambil antrian -->
    <div class="row " style="height: 538px; margin-left: 20rem;">
        <div class="col-md  mt-3 ">
            <div class="row card-antrian text-center ">
                <div class="card border-primary mb-3 shadow-lg" style="max-width: 20rem;">
                    <div class="card-header"> <i class="fa fa-fw fa-capsules"></i>Obat Non Racikan</div>
                    <div class="card-body text-primary">
                        <h5 class="card-title" id="nomer1">A</h5>
                        <form action="<?= base_url('antrian/tambah_nonracik/') ?>" method="post">
                            <input type="hidden" name="non_racik" id="non_racik" value="">
                            <button type="submit" class="btn btn-primary ">Print Antrian</button>
                        </form>

                    </div>

                </div>

                <div class="card border-primary mb-3 ml-5 shadow-lg" style="width: 20rem;">
                    <div class="card-header"> <i class="fa fa-fw fa-mortar-pestle"></i> Obat Racikan</div>
                    <div class="card-body text-primary">
                        <h5 class="card-title" id="nomer2">B</h5>
                        <form action="<?= base_url('antrian/tambah_racik/') ?>" method="post">
                            <input type="hidden" name="racik" id="racik" value="">
                            <button type="submit" class="btn btn-primary ">Print Antrian</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- akhir ambil antrian -->

</div>
<!-- End of Main Content -->
<script type="text/javascript">
    function nomer1() {
        var antri = parseInt(document.getElementById('nomer1').innerHTML) + 1;
        document.getElementById("nomer1").innerHTML = antri;
    }

    function nomer2() {
        var antri = parseInt(document.getElementById('nomer2').innerHTML) + 1;
        document.getElementById("nomer2").innerHTML = antri;
    }

    setInterval(function() {

        $.ajax({
            url: '<?= base_url(); ?>/antrian/ambil_antri_nonracik/',
            method: "post",
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                if (data.no_antrian == null) {

                    document.getElementById("nomer1").innerHTML = "A 0";
                    document.getElementById("non_racik").value = 0;
                } else {
                    document.getElementById("nomer1").innerHTML = "A" + data.no_antrian;
                    document.getElementById("non_racik").value = data.no_antrian;

                }

            }
        })

        $.ajax({
            url: '<?= base_url(); ?>/antrian/ambil_antri_racik/',
            method: "post",
            dataType: 'json',
            success: function(data) {


                if (data.no_antrian == null) {

                    document.getElementById("nomer2").innerHTML = "B 0";
                    document.getElementById("racik").value = 0;

                } else {

                    document.getElementById("nomer2").innerHTML = "B" + data.no_antrian;
                    document.getElementById("racik").value = data.no_antrian;
                }


            }
        });
    }, 1000);
</script>