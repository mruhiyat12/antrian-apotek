<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->

    <h1 class=" mb-2 text-gray-900 text-center" style="font-family: 'Lobster', cursive;"><?= $title; ?></h1>

    <div class="row justify-content-center" style="height: 538px;">
        <!-- slide -->

        <!-- akhir slide -->
        <div class="">
            <div class="row  card-antrian text-center  ">




                <div class="card border-primary mb-3 shadow-lg" style="max-width: 18rem;">
                    <div class="card-header"> <i class="fa fa-fw fa-capsules"></i>Obat Non Racikan</div>
                    <div class="card-body text-primary">

                        <h5 class="card-title" id="antrian_nonracik"></h5>
                        <script type="text/javascript">
                            setInterval(function() {

                                $.ajax({
                                    url: '<?= base_url(); ?>/antrian/get_antri_nonracik/',
                                    method: "post",
                                    dataType: 'json',
                                    success: function(data) {

                                        if (data.no_antrian == null) {

                                            document.getElementById("antrian_nonracik").innerHTML = "A 0";
                                        } else {
                                            document.getElementById("antrian_nonracik").innerHTML = "A" + data.no_antrian;

                                        }

                                    }
                                })
                            }, 1000);
                        </script>

                    </div>
                </div>




                <div class="card border-primary mb-3 ml-5 shadow-lg" style="width: 18rem;">
                    <div class="card-header"> <i class="fa fa-fw fa-mortar-pestle"></i> Obat Racikan</div>
                    <div class="card-body text-primary">

                        <h5 class="card-title" id="antrian_racik"></h5>
                        <script type="text/javascript">
                            setInterval(function() {

                                $.ajax({
                                    url: '<?= base_url(); ?>/antrian/get_antri_racik/',
                                    method: "post",
                                    dataType: 'json',
                                    success: function(data) {


                                        if (data.no_antrian == null) {

                                            document.getElementById("antrian_racik").innerHTML = "B 0";

                                        } else {

                                            document.getElementById("antrian_racik").innerHTML = "B" + data.no_antrian;

                                        }


                                    }
                                });
                            }, 1000);
                        </script>


                    </div>

                </div>

            </div>
        </div>



    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->